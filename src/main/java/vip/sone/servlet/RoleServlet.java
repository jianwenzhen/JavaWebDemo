package vip.sone.servlet;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.junit.Assert;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/role")
public class RoleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Subject subject = SecurityUtils.getSubject();
//        Assert.assertTrue(subject.isAuthenticated());
        try {
            subject.checkRole("admin");
        } catch (Exception e) {
            resp.setContentType("text/html;charset=UTF-8");
            resp.getWriter().write("木有权限，回家去吧！");
            return;
        }
        req.getRequestDispatcher("/jsp/hasRole.jsp").forward(req, resp);
    }
}
