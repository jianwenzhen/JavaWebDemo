package vip.sone.servlet;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/form/login")
public class FormLoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String error = null;
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        try {
            subject.login(token);
        } catch (UnknownAccountException e) {
            error = "用户名或密码错误";
        } catch (IncorrectCredentialsException e) {
            error = "用户名或密码错误";
        } catch (AuthenticationException e) {
            error = "其他错误: " + e.getMessage();
        }

        String errorClassName = (String) req.getAttribute("shiroLoginFailure");
        if (UnknownAccountException.class.getName().equals(errorClassName) || IncorrectCredentialsException.class.getName().equals(errorClassName)) {
            error = "用户名或密码错误！";
        } else if (null != errorClassName) {
            error = "未知错误" + errorClassName;
        }
        req.setAttribute("error", error);
        req.getRequestDispatcher("/jsp/form/login.jsp").forward(req, resp);
    }
}
