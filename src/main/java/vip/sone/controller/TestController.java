package vip.sone.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

/**
 * @author SOneWinstone jianwenzhen@qq.com
 */
@Api(description = "测试Controller")
@RestController
@RequestMapping("/app")
public class TestController {

    @GetMapping("/test")
    @ApiOperation(value = "测试", notes = "测试方法", response = String.class)
    public String test() {
        return "Hello APP!";
    }

    @PostMapping("/login")
    @ApiOperation(value = "登录", notes = "登录验证", response = String.class)
    public String login(@ApiParam(value = "用户名") @RequestBody String username,
                        @ApiParam(value = "密码") @RequestBody String password) {
        System.out.println(username + "---" + password);
        return "login";
    }
}