package vip.sone.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import vip.sone.util.RSAUtil;

import javax.servlet.http.HttpSession;
import java.security.KeyPair;
import java.security.PublicKey;

/**
 * @author SOneWinstone jianwenzhen@qq.com
 */
@RequestMapping("/rsa")
//@SessionAttribute(value = "")
public class RSAController {

    @GetMapping
    public PublicKey getPublicKey(HttpSession session) {
        KeyPair keyPair = RSAUtil.generateRSAKeyPair();
        session.setAttribute("privateKey", keyPair.getPrivate());
        return keyPair.getPublic();
    }
}
