package vip.sone.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Role {
    private Long id;
    private String role;
    private String description;
    private Boolean available = Boolean.TRUE;
}
