package vip.sone.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Permission {
    private Long id;
    private String permission;
    private String description;
    private Boolean available = Boolean.TRUE;
}
