package vip.sone.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolePermission {
    private Long rid;
    private Long pid;
}
