package vip.sone.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author SOneWinstone jianwenzhen@qq.com
 */
@Getter
@Setter
public class User {
    private Long id;
    private Date birthday;
    private boolean sex;
    private String email;
    private String phone;
    private String nickname;

    private String username;
    private String password;
    // 盐
    private String salt;
    // 是否锁定
    private Boolean locked = Boolean.FALSE;

    public String getCredentialsSalt() {
        return username + salt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", locked=" + locked +
                '}';
    }
}
