package vip.sone.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRole {
    private Long rid;
    private Long uid;
}
