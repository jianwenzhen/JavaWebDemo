package vip.sone.shiro.filter;

import org.apache.shiro.web.servlet.AdviceFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 扩展AdviceFilter
 *
 * AdviceFilter提供了AOP功能
 */
public class MyAdviceFilter extends AdviceFilter {

    /**
     * 前置处理
     *
     * 在Filter真正executed/consulted(执行)前调用
     *
     * 返回true继续向下执行Filter 其他情况返回false
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("========================== 前置处理 ==========================");
        return true;
    }

    /**
     * 后置处理
     *
     * 在Filter执行后调用， 除非发生了Exception
     */
    @Override
    protected void postHandle(ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("========================== 后置处理 ==========================");
    }

    /**
     * 最终处理
     *
     * 不论preHandle有没有返回false或者Filter执行的时候有没有发生异常，这个方法都会执行 可以在这里做资源回收清理
     */
    @Override
    public void afterCompletion(ServletRequest request, ServletResponse response, Exception exception) throws Exception {
        System.out.println("========================== 无论如何都处理处理 ==========================");
    }
}
