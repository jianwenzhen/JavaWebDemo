package vip.sone.shiro.filter;

import org.apache.shiro.web.servlet.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * 扩展OncePerRequestFilter
 *
 * OncePerRequestFilter保证一次请求只调用一次doFilterInternal
 */
public class MyOncePerRequestFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        System.out.println("========================== MyOncePerRequestFilter ==========================");
        chain.doFilter(request, response);
    }
}
