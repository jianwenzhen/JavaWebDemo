package vip.sone.shiro.filter;

import org.apache.shiro.web.filter.AccessControlFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * AccessControlFilter继承了PathMatchingFilter 并扩展了两个方法
 */
public class MyAccessControlFilter extends AccessControlFilter {
    @Override
    public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        return isAccessAllowed(request, response, mappedValue) || onAccessDenied(request, response, mappedValue);
    }

    /**
     * 是否允许访问
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        System.out.println("Access Allowed!");
        return true;
    }

    /**
     * 表示访问拒绝时是否自己处理，如果返回true表示自己不处理且继续过滤器链执行，返回false表示自己已经处理了(比如重定向到另一个页面)
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("访问失败也不自己处理，继续过滤器链的执行");
        return true;
    }
}
