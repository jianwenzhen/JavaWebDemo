package vip.sone.shiro.filter;

import org.apache.shiro.web.filter.PathMatchingFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * PathMatchingFilter继承了AdviceFilter，提供了url模式过滤的功能，如果需要对特定的请求进行处理，可以扩展PathMatchingFilter
 */
public class MyPathMatchingFilter extends PathMatchingFilter {
    /**
     * 会进行 url 模式与请求 url 进行匹配，如果匹配会调用 onPreHandle；如果没有配置 url 模式/没有 url 模式匹配
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        System.err.println("****************** 测试一下这里有用没 ******************");
        return super.preHandle(request, response);
    }

    /**
     * 如果 url 模式与请求 url 匹配，那么会执行 onPreHandle，并把该拦截器配置的参数传入
     */
    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
//        return super.onPreHandle(request, response, mappedValue);

        String[] objects = (String[]) mappedValue;
        int i = 0;
        for (String str : objects) {
            System.out.println("URL" + ++i + " : " + str);
            if (str.contains("config")) {
                System.err.println("真的有用诶！");
            }
        }
        System.out.println("Url matches, config is " + Arrays.toString((String[])mappedValue));
        return true;
    }
}
