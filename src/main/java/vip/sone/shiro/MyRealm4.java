package vip.sone.shiro;

import org.apache.shiro.authc.*;
import org.apache.shiro.realm.Realm;

public class MyRealm4 implements Realm{
    @Override
    public String getName() {
        return "myrealm4";
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        // 仅支持用户名密码Token
        return token instanceof UsernamePasswordToken;
    }

    /**
     * 返回认证信息
     */
    @Override
    public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        // 得到用户名和密码 只有基于用户名和密码认证的Token可以这样
        String username = (String) token.getPrincipal();
        // 注意这里的强转
        String password = new String((char[])token.getCredentials());
        if (!"zhang".equals(username)) {
            throw new UnknownAccountException();
        }
        if (!"123".equals(password)) {
            throw new IncorrectCredentialsException();
        }
        // 认证成功就返回一个AuthenticationInfo实现
        return new SimpleAuthenticationInfo("zhang@aliyun.com", password, getName());
    }
}
