package vip.sone.service;

import vip.sone.entity.User;

import java.util.Set;

public interface UserService {
    public User createUser(User user);

    public void changePassword(Long uid, String password);

    public void correlationRoles(Long uid, Long... rids);

    public void uncorrelationRoles(Long uid, Long... rids);

    public User findByUsername(String username);

    public Set<String> findRoles(String username);

    public Set<String> findPermissions(String username);
}
