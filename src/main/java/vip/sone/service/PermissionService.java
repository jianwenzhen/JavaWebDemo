package vip.sone.service;

import vip.sone.entity.Permission;

public interface PermissionService {
    public Permission createPermission(Permission permission);

    public void deletePermission(Long id);
}
