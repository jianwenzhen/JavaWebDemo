package vip.sone.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.sone.common.PasswordHelper;
import vip.sone.dao.UserDao;
import vip.sone.entity.User;
import vip.sone.service.UserService;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private PasswordHelper passwordHelper;
    @Autowired
    private UserDao userDao;

    @Override
    public User createUser(User user) {
        passwordHelper.encryptPassword(user);
        return userDao.createUser(user);
    }

    @Override
    public void changePassword(Long uid, String password) {
        User user =userDao.findOne(uid);
        user.setPassword(password);
        passwordHelper.encryptPassword(user);
        userDao.updateUser(user);
    }

    @Override
    public void correlationRoles(Long uid, Long... rids) {
        userDao.correlationRoles(uid, rids);
    }

    @Override

    public void uncorrelationRoles(Long uid, Long... rids) {
        userDao.uncorrelationRoles(uid, rids);
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public Set<String> findRoles(String username) {
        return userDao.findRoles(username);
    }

    @Override
    public Set<String> findPermissions(String username) {
        return userDao.findPermissions(username);
    }
}
