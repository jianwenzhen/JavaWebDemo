package vip.sone.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.sone.dao.RoleDao;
import vip.sone.entity.Role;
import vip.sone.service.RoleServcie;

@Service
public class RoleServiceImpl implements RoleServcie {
    @Autowired
    private RoleDao roleDao;

    @Override
    public Role createRole(Role role) {
        return roleDao.createRole(role);
    }

    @Override
    public void deleteRole(Long id) {
        roleDao.deleteRole(id);
    }

    @Override
    public void correlationPermissions(Long rid, Long... pids) {
        roleDao.correlationPermissions(rid, pids);
    }

    @Override
    public void uncorrelationPermissions(Long rid, Long... pids) {
        roleDao.uncorrelationPermissions(rid, pids);
    }
}
