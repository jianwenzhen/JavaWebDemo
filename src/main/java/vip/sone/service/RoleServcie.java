package vip.sone.service;

import vip.sone.entity.Role;

public interface RoleServcie {
    public Role createRole(Role role);

    public void deleteRole(Long id);

    /**
     * 添加角色-权限之间的关系
     */
    public void correlationPermissions(Long rid, Long... pids);

    /**
     * 解除角色-权限之间的关系
     */
    public void uncorrelationPermissions(Long rid, Long... pids);
}
