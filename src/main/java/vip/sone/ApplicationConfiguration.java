package vip.sone;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author SOneWinstone jianwenzhen@qq.com
 */
@Configuration
@EnableSwagger2
public class ApplicationConfiguration {

//    public static void main(String[] args) {
//        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-config.ini");
//
//        SecurityManager securityManager = factory.getInstance();
//        SecurityUtils.setSecurityManager(securityManager);
//
//        Subject subject = SecurityUtils.getSubject();
//        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");
//        subject.login(token);
//        Assert.assertTrue(subject.isAuthenticated());
//    }

    @Bean
    public Docket createRESTApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("vip.sone.controller"))
                .paths(PathSelectors.any())
//                .paths(PathSelectors.ant("/test/**"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Test APIs")
                .description("测试的RESTful API接口")
                .termsOfServiceUrl("https://blog.sone.vip/")
                .contact(new Contact("SoneWinstone", "https://blog.sone.vip", "jianwenzhen@qq.com"))
                .version("1.1")
                .build();
    }
}
