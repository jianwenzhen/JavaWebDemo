package vip.sone.util;

import org.junit.Test;

import java.io.*;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

/**
 * @author SOneWinstone jianwenzhen@qq.com
 */
public class RSAUtilTest {
    private static final String DATA = "Hello RSA!";
    private static final int BUFFER_SIZE = 1024;

    @org.junit.Before
    public void setUp() throws Exception {
        System.out.println("**************** Start Test ****************");
    }

    @Test
    public void base64encode() {
        String data = "58135de8d86017667edaf59f9a23e8c0248e3347e95248902788f3f9af7b361cb342bda0ef4cfa004617d289ed05b84807684bbf61a128f4f0c10088185e2a2bbf7c7ecbbd0ea603150a6f55c5d9201b8dc5eb8747a1194642fd359cc9f2db0117b13dcaa490077a1f1786ef08a7b171209406a54222a041864978e6cb800d4b";
        byte[] encode = Base64.getEncoder().encode(data.getBytes());
        System.out.println(new String(encode));
        String encrypt = "NTgxMzVkZThkODYwMTc2NjdlZGFmNTlmOWEyM2U4YzAyNDhlMzM0N2U5NTI0ODkwMjc4OGYzZjlhZjdiMzYxY2IzNDJiZGEwZWY0Y2ZhMDA0NjE3ZDI4OWVkMDViODQ4MDc2ODRiYmY2MWExMjhmNGYwYzEwMDg4MTg1ZTJhMmJiZjdjN2VjYmJkMGVhNjAzMTUwYTZmNTVjNWQ5MjAxYjhkYzVlYjg3NDdhMTE5NDY0MmZkMzU5Y2M5ZjJkYjAxMTdiMTNkY2FhNDkwMDc3YTFmMTc4NmVmMDhhN2IxNzEyMDk0MDZhNTQyMjJhMDQxODY0OTc4ZTZjYjgwMGQ0Yg==";
        byte[] decode = Base64.getDecoder().decode(encrypt);
        System.out.println(new String(decode));
    }

    @Test
    public void generateKey() throws IOException {
        KeyPair keyPair = RSAUtil.generateRSAKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        // 将秘钥以Base64格式写入文件
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("pkcs8_private_key.pem")));
        writer.write(Base64.getEncoder().encodeToString(privateKey.getEncoded()));
        writer.close();
        writer = new BufferedWriter(new FileWriter(new File("rsa_public_key.pem")));
        writer.write(Base64.getEncoder().encodeToString(publicKey.getEncoded()));
        writer.close();
    }

    @Test
    public void encrypt() throws IOException {
        PublicKey publicKey = RSAUtil.restorePublicKey(new FileInputStream(new File("rsa_public_key.pem")));
        final String data = "test";
//        byte[] bytes = RSAUtil.encryptData(DATA.getBytes(), publicKey);
        byte[] bytes = RSAUtil.encryptData(data.getBytes(), publicKey);
        assert(bytes != null);
        String encodeData = Base64.getEncoder().encodeToString(bytes);
//        System.out.println("加密：" + new String(bytes));
        System.out.println("加密：" + encodeData);

//        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(getClass().getClassLoader().getResource("").getPath() + "/encrypt")));
//        out.write(bytes);
//        out.close();
        File file = new File(getClass().getClassLoader().getResource("").getPath() + "/encrypt");
        System.out.println("File: " + file.getAbsolutePath());
        BufferedWriter out = new BufferedWriter(new FileWriter(file));
        out.write(encodeData);
        out.close();
    }

    @Test
    public void decrypt() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File("pkcs8_private_key.pem")));
        String line = null;
        StringBuilder builder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();
        String privateKeyStr = builder.toString();

        BufferedInputStream in = new BufferedInputStream(new FileInputStream(new File(getClass().getClassLoader().getResource("").getPath() + "/encrypt")));
        byte[] buf = new byte[BUFFER_SIZE];
        int cnt = 0;
        int read;
        while ((read = in.read(buf)) == BUFFER_SIZE) {
            cnt += BUFFER_SIZE;
        }
        cnt += read;
        in.close();
        byte[] data = new byte[cnt];
        for (int i = 0; i < cnt; i++) {
            data[i] = buf[i];
        }

        byte[] decode = Base64.getDecoder().decode(data);
        byte[] bytes = RSAUtil.decryptData(decode, RSAUtil.restorePrivateKey(privateKeyStr));
        assert bytes != null;
        System.out.println("解密：" + new String(bytes));
    }

    @org.junit.After
    public void tearDown() throws Exception {
        System.out.println("**************** Stop  Test ****************");
    }

}