package vip.sone.common;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;

public class BaseTest {
    public void login(String configFile, String username, String password) {
//        String path = getClass().getClassLoader().getResource(configFile).getPath();
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<SecurityManager> factory = new IniSecurityManagerFactory(configFile);
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        subject.login(token);
    }

    public Subject subject() {
        return SecurityUtils.getSubject();
    }
}
