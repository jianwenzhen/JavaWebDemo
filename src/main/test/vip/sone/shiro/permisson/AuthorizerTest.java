package vip.sone.shiro.permisson;

import org.junit.Assert;
import org.junit.Test;
import vip.sone.common.BaseTest;

public class AuthorizerTest extends BaseTest {
    private final String username = "zhang";
    private final String password = "123";

    @Test
    public void isPermitted() {
        login("shiro-authorizer.ini", username, password);

        Assert.assertTrue(subject().isPermitted("user1:update"));
        Assert.assertTrue(subject().isPermitted("user2:update"));

        Assert.assertTrue(subject().isPermitted("+user1+10"));
        Assert.assertTrue(subject().isPermitted("+user1+2"));
        Assert.assertTrue(subject().isPermitted("+user1+8"));
        Assert.assertTrue(subject().isPermitted("+user2+10"));
        Assert.assertTrue(subject().isPermitted("+user1+4"));

        Assert.assertTrue(subject().isPermitted("menu:view"));
    }
}
