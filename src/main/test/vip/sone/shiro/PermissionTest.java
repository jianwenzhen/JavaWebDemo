package vip.sone.shiro;

import org.junit.Assert;
import org.junit.Test;
import vip.sone.common.BaseTest;

public class PermissionTest extends BaseTest {
    @Test
    public void isPermitted() {
        String username = "zhang";
        String password = "123";
        login("shiro-permission.ini", username, password);

        Assert.assertTrue(subject().isPermitted("user:create"));
        Assert.assertTrue(subject().isPermittedAll("user:create", "user:delete"));
        // Assert.assertTrue(subject().isPermitted("user:view"));
    }

    @Test/*(expected = UnauthorizedException.class)*/
    public void checkPermitted() {
        String username = "wang";
        String password = "123";
        login("shiro-permission.ini", username, password);

        subject().checkPermission("user:create");
        subject().checkPermissions("user:create", "user:delete");
        // 下面这句失败了 运行成功 但会抛出异常
        subject().checkPermissions("user:view");

        subject().checkPermission("user:*");
        subject().checkPermission("user");
        subject().checkPermission("user:create,update");
        subject().checkPermission("user:create,update,view,user:delete");
        subject().checkPermissions("system:user:view", "system:view");
        subject().checkPermissions("system:user:update", "system:view");
    }
}
