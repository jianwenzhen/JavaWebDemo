package vip.sone.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.junit.Test;

public class LoginLogoutTest {
    @Test
    public void HelloWorld() {
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(this.getClass().getClassLoader().getResource("shiro.ini").getPath());
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");
        try {
            subject.login(token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(true, subject.isAuthenticated());

        subject.logout();
    }

    @Test
    public void myRealm1() {
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(this.getClass().getClassLoader().getResource("shiro-realm.ini").getPath());
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");
        try {
            subject.login(token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(true, subject.isAuthenticated());

        subject.logout();
    }

    @Test
    public void multiRealms() {
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(this.getClass().getClassLoader().getResource("shiro-multi-realm.ini").getPath());
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        // UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");
        // UsernamePasswordToken token = new UsernamePasswordToken("wang", "123");
        UsernamePasswordToken token = new UsernamePasswordToken("wang1", "123");
        try {
            subject.login(token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(true, subject.isAuthenticated());

        subject.logout();
    }

    @Test
    public void jdbcRealm() {
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(this.getClass().getClassLoader().getResource("shiro-jdbc-realm.ini").getPath());
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");
        try {
            subject.login(token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(true, subject.isAuthenticated());

        subject.logout();
    }

    @Test
    public void allSuccessfulStrategy() {
        login(getClass().getClassLoader().getResource("shiro-authenticator-all-success.ini").getPath());
        Subject subject = SecurityUtils.getSubject();

        //得到一个身份集合，其包含了 Realm 验证成功的身份信息
        PrincipalCollection principals = subject.getPrincipals();
        Assert.assertEquals(2, principals.asList().size());
    }

    @Test
    public void allSuccessfulStrategyWithFail() {
        login(getClass().getClassLoader().getResource("shiro-authenticator-all-fail.ini").getPath());
        Subject subject = SecurityUtils.getSubject();

        //得到一个身份集合，其包含了 Realm 验证成功的身份信息
        PrincipalCollection principals = subject.getPrincipals();
        Assert.assertEquals(2, principals.asList().size());
    }

    @Test
    public void AtLeastTwoStrategy() {
        login("classpath:shiro-authenticator-least2.ini");
        Subject subject = SecurityUtils.getSubject();

        //得到一个身份集合，其包含了 Realm 验证成功的身份信息
        PrincipalCollection principals = subject.getPrincipals();
        for (Object principal : principals.asList()) {
            System.out.println("principal: " + (String) principal);
        }
        Assert.assertEquals(3, principals.asList().size());
    }

    private void login(String configFile) {
        // 1. 通过ini文件初始化SecurityManagerFactory
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(configFile);
        // 2. 得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager manager = factory.getInstance();
        SecurityUtils.setSecurityManager(manager);
        //3. 得到Subject及创建用户名/密码身份验证Token
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");

        subject.login(token);
    }
}
