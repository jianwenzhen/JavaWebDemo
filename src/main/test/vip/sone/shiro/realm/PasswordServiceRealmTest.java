package vip.sone.shiro.realm;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean2;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.junit.Test;
import vip.sone.common.BaseTest;

import static org.junit.Assert.*;

public class PasswordServiceRealmTest extends BaseTest {
    @Test
    public void doGetAuthenticationInfo() throws Exception {
        login("classpath:shiro-passwordService.ini", "wu", "123");
    }

    @Test
    public void testHashedCredentialsMatcherWithJdbcRealm() {
//        BeanUtilsBean.getInstance().getConvertUtils().register(new EnumConverter(), JdbcRealm.SaltStyle.class);
        //使用testGeneratePassword生成的散列密码
        login("classpath:shiro-jdbc-hashedCredentialsMatcher.ini", "liu", "123");
    }
}