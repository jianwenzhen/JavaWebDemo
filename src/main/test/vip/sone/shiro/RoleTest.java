package vip.sone.shiro;

import org.junit.Assert;
import org.junit.Test;
import vip.sone.common.BaseTest;

import java.util.Arrays;

public class RoleTest extends BaseTest {

    @Test
    public void hasRole() {
        String username = "zhang";
        String password = "123";
        login(getClass().getClassLoader().getResource("shiro-role.ini").getPath(), username, password);

        Assert.assertTrue(subject().hasRole("role1"));
        Assert.assertTrue(subject().hasAllRoles(Arrays.asList("role1", "role2")));
        boolean[] booleans = subject().hasRoles(Arrays.asList("role1", "role2", "role3"));
        Assert.assertTrue(booleans[0]);
        Assert.assertTrue(booleans[1]);
        // Assert.assertTrue(booleans[2]);
    }

    @Test
    public void checkRole() {
        String username = "zhang";
        String password = "123";
        login("shiro-role.ini", username, password);

        subject().checkRole("role1");
        subject().checkRoles("role1", "role2");
        //subject().checkRoles("role1", "role3");
    }
}
